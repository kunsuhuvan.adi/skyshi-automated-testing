from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import ActionChains
import time

driver = webdriver.Chrome(executable_path="C:\\Users\\HP\\Downloads\\chromedriver.exe")
print(type(driver))
driver.maximize_window()

#halaman utama
driver.get('https://ivan-todo-devrank.netlify.app/')
driver.implicitly_wait(10)
#render header
try:
    WebDriverWait(driver,10).until(EC.text_to_be_present_in_element((By.CSS_SELECTOR, 'h2[data-cy=header-title]'), 'TO DO LIST APP'))
    print('Render Header Tittle')

except TimeoutException:
    print('Tidak Muncul')
    pass

#Expect Tittle Activity
try:
    WebDriverWait(driver,10).until(EC.text_to_be_present_in_element((By.CSS_SELECTOR, 'h1[data-cy=activity-title]'), 'Activity'))
    print('Expected Activity Tittle')

except TimeoutException:
    print('Tidak Muncul')
    pass

#Expect Button Tambah
try:
    WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'div[data-cy=activity-item]')))
    print('Expected Activity Item')

except TimeoutException:
    print('Tidak Muncul')
    pass


#Expect Activity Item
try:
    WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[data-cy=activity-add-button]')))
    print('Expected Activity Item')

except TimeoutException:
    print('Tidak Muncul')
    pass

#Tambah Activity
#Tambah Data Baru
driver.find_element_by_css_selector('button[data-cy=activity-add-button]').click()
if True:
     print('Tambah Activity Baru')


#Expect Tittle Item Activity
try:
    WebDriverWait(driver,10).until(EC.text_to_be_present_in_element((By.CSS_SELECTOR, 'h4[data-cy=activity-item-title]'), 'New Activity'))
    print('Expected Tittle Activity Item')

except TimeoutException:
    print('Expected Tittle Activity Item FAil')
    pass

#Expect Date Item Activity
try:
    WebDriverWait(driver,10).until(EC.text_to_be_present_in_element((By.CSS_SELECTOR, 'span[data-cy=activity-item-date]'), '10 April 2022'))
    print('Expected Item Date')

except TimeoutException:
    print('Expected Item Date Fail')
    pass

#Hapus Activity
#Expect Button Delete
try:
    button_delete = WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'img[data-cy=activity-item-delete-button]')))
    print('Expected Button Delete')
    button_delete.click()

except TimeoutException:
    print('Expected Button Delete Failed')

#Expect Modal Hapus
try:
    modal_delete = WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'div[data-cy=todo-modal-delete]')))
    print('Expected Modal Delete')

except TimeoutException:
    print('Expected Modal Delete Failed')

#Batal Hapus Dengan Klik Tombol Batal
driver.find_element_by_css_selector('button[data-cy=modal-delete-cancel-button]').click()

#Expected Modal hapus Non Exist/Non Visible
try:
    WebDriverWait(driver,10).until(EC.invisibility_of_element((By.CSS_SELECTOR, 'div[data-cy=todo-modal-delete]')))
    print('Expected Non Visible Modal ')

except TimeoutException:
    print('Expected Non visible Modal Failed')


#Batal Hapus Dengan Klik di Luar Modal
#Pastikan Modal Delete Terbuka
button_delete.click()
modal_delete
print('Modal Delete Terbuka')

#Batal Dengan Klik diluar Modal
try:
    luar_modal = WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'div[role=dialog]')))
    print('Batal Klik Diluar Modal')
    luar_modal.click()

except TimeoutException:
    print('Batal Klik Diluar Modal Failed')
    pass

#Klik Button Hapus
driver.find_element_by_css_selector('button[data-cy=modal-delete-confirm-button]').click()
print('Click Button Hapus')

#Memastikan Item Berhasil Dihapus
try:
    berhasil_hapus = WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'p[data-cy=modal-information-title]')))
    berhasil_hapus.click()
    print('Berhasil Hapus')

except TimeoutException:
    print("Tidak Berhasil Hapus")
    pass

#Menampilkan Detail Activity
try:
    item_activity =  WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'div[data-cy=activity-item]')))
    print('Render Item Activity')
    item_activity.click()

except TimeoutException:
    print('Render Item Activity FAil')
    pass

#Expect Text pada TittleTODO
try:
    WebDriverWait(driver,10).until(EC.text_to_be_present_in_element((By.CSS_SELECTOR, 'h1[data-cy=todo-title]'), 'New Activity'))
    print('Expected Todo Tittle')

except TimeoutException:
    print('Expect Todo Tittle Tidak Muncul')
    pass

#Expect Button Tambah
try:
    WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[data-cy=todo-add-button]')))
    print('Expected Button  Tambah')

except TimeoutException:
    print('Expect Button Tambah Fail')
    pass

#Expect Empty State
try:
    WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'div[data-cy=todo-empty-state]')))
    print('Expected Empty State')

except TimeoutException:
    print('Expect Empty State Fail')
    pass

#Mengganti TitleTODO
driver.find_element_by_css_selector('div[data-cy=todo-title-edit-button]').click()
try:
    todo_title = driver.find_element_by_css_selector('input[type=text]')
    time.sleep(5)
    todo_title.send_keys(Keys.CONTROL + 'a')
    time.sleep(5)
    todo_title.send_keys('Activity Baru')
    time.sleep(5)
    print ( 'Klik Todo Title dan Ubah Title' )

except TimeoutException:
    print('klik Todo Title dan Ubah Title Failed')

driver.find_element_by_css_selector('h2[data-cy=header-title').click()

#Expect Text pada Tittle yang Berubah
try:
    WebDriverWait(driver,10).until(EC.text_to_be_present_in_element((By.CSS_SELECTOR, 'h1[data-cy=todo-title]'), 'Activity Baru'))
    print('Expected Tittle yang Berubah')

except TimeoutException:
    print('Expect Tittle yang Berubah Failed')
    pass

#Menambah Beberapa Item T0d0
#Click Button Tambah
klik_button_tambah = WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[data-cy=todo-add-button]')))
klik_button_tambah.click()
print('Click Button Tambah Item')

#Expect Modal tambah Item
try:
    WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'label[data-cy=modal-add-name-title]')))
    print('Expected Modal Tambah Item')
    time.sleep(10)

except TimeoutException:
    print('Expect Modal Tambah Item Failed')
    pass
time.sleep(10)

#Click Form Nama Item dan Ketikkan Nama Item
WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'div[data-cy=modal-add-name-input]'))).click()
time.sleep(5)
try:
    nama_item = WebDriverWait ( driver, 10 ).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'input[id=AddFormTitle]')))
    nama_item.send_keys('Coba Item 1')
    time.sleep(5)
    print("Ketik Nama Item")
except TimeoutException:
    print('Ketik nama Item Failed')
    pass

#Click Dropdown Priority
pilih_listDropdown = driver.find_element_by_css_selector ('div[id=AddFormPriority]')
pilih_listDropdown.click()
time.sleep(5)

actions = ActionChains(driver)
time.sleep(5)
actions.send_keys(Keys.ARROW_DOWN)
time.sleep(5)
actions.send_keys(Keys.ENTER)
time.sleep(5)
actions.perform()
time.sleep(5)
print('Pilih Dropdown 1')

#Tambah Data Baru
#Click Dengan Button Simpan
driver.find_element_by_css_selector('button[data-cy=modal-add-save-button]').click()
time.sleep(5)
print('Click Button Simpam')

#Memastikan Jumlah Item bertambah
try:
    WebDriverWait(driver,10).until(EC.text_to_be_present_in_element((By.CSS_SELECTOR, 'span[data-cy=todo-item-title]'), 'Coba Item 1'))
    print('Item Berhasil Tambah')

except TimeoutException:
    print('Item Berhasil Tambah Failed')
    pass

#Buka Lagi Modal Tambah Item
klik_button_tambah = WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[data-cy=todo-add-button]')))
klik_button_tambah.click()
print('Ulang Click Button Tambah Item')

#Ulang Click Form Nama Item dan Ketikkan Nama Item
WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'div[data-cy=modal-add-name-input]'))).click()
time.sleep(5)
try:
    nama_item = WebDriverWait ( driver, 10 ).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'input[id=AddFormTitle]')))
    nama_item.send_keys('Tes Item 2 Untuk Selenium Python')
    time.sleep(5)
    print("Ulang Ketik Nama Item dengan Inputan Panjang")
except TimeoutException:
    print('Ulang Ketik nama Item Failed')
    pass

#Ulang Pilih Dropdown Dengan Dropdown Berbeda
pilih_listDropdown = driver.find_element_by_css_selector ('div[id=AddFormPriority]')
pilih_listDropdown.click()
time.sleep(5)

actions = ActionChains(driver)
time.sleep(5)
actions.send_keys(Keys.ARROW_DOWN)
time.sleep(5)
actions.send_keys(Keys.ARROW_DOWN)
time.sleep(5)
actions.send_keys(Keys.ENTER)
time.sleep(5)
actions.perform()
time.sleep(5)
print('Pilih Dropdown Berbeda, Dropdown Index ke-2')

#Ulangi Step Click Dengan Button Simpan
driver.find_element_by_css_selector('button[data-cy=modal-add-save-button]').click()
time.sleep(5)
print('Ulang Click Button Simpam')

#Ulang Buka Modal Tambah Item, Mengkosongkan Title Item dan Pilih Dropdown ke 4 Kemudian Click Simpan
klik_button_tambah = WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[data-cy=todo-add-button]')))
klik_button_tambah.click()
print('Ulang Click Button Tambah Item')

pilih_listDropdown = driver.find_element_by_css_selector ('div[id=AddFormPriority]')
pilih_listDropdown.click()
time.sleep(5)
actions = ActionChains(driver)
time.sleep(5)
actions.send_keys(Keys.ARROW_DOWN)
time.sleep(5)
actions.send_keys(Keys.ARROW_DOWN)
time.sleep(5)
actions.send_keys(Keys.ARROW_DOWN)
time.sleep(5)
actions.send_keys(Keys.ARROW_DOWN)
time.sleep(5)
actions.send_keys(Keys.ENTER)
time.sleep(5)
actions.perform()
time.sleep(5)
print('Dropdown Index ke-4')

try:
    WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[data-cy=modal-add-save-button]'))).click()
    print('Bug, karena data tidak bisa ditambahkan. Field title mandatory')

except TimeoutException:
    print('Click Button Tambah Tidak Berfungsi. SESUAI, karena field title yang mandatory tidak diisi')
    pass


#Menyortir Item T0d0
#Excpect Render Sort Item List
WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'div[data-cy=modal-add-close-button'))).click()


try:
    WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[data-cy=todo-sort-button]'))).click()
    print('Expected Button Sort Item')

except TimeoutException:
    print('Expect Button Sorl Item Failed')
    pass

time.sleep(5)
#Click Button Sort dan Click Sort terlama, A-Z, Z-A
WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/div[2]/div/div[2]/ul/a[2]'))).click()
print('Sort Terlama')
time.sleep(5)
WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[data-cy=todo-sort-button]'))).click()
WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/div[2]/div/div[2]/ul/a[3]'))).click()
print('Sort Item A-Z')
time.sleep(5)
WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[data-cy=todo-sort-button]'))).click()
WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/div[2]/div/div[2]/ul/a[4]'))).click()
print('Sort Item Z-A')
time.sleep(5)

#Menandai Selesai Item T0d0
#Click Checkbox
WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'input[data-cy=todo-item-checkbox]'))).click()
print('Click Checkbox')
time.sleep(5)
#Menampilkan Sort Belum Selesai
WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[data-cy=todo-sort-button]'))).click()
WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/div[2]/div/div[2]/ul/a[5]'))).click()
print('Sort Belum Selesai')

#menghapus Item T0d0
#Cek Render Button Hapus Item Activity
try:
    hapus_itemActivity = WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'img[data-cy=todo-item-delete-button]')))
    print ( 'Expected Button Hapus Item Activity' )
    hapus_itemActivity.click ()
    print('Click Button Hapus Activity')

except TimeoutException:
    print('Expect Button Hapus Item Activity Failed')
    pass

#Expected Modal Hapus
try:
    hapus_itemActivity = WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'div[data-cy=todo-modal-delete]')))
    print ( 'Expected Modal  Hapus Item Activity' )


except TimeoutException:
    print('Expect Modal Hapus Item Activity Failed')
    pass

#Click Button Hapus pada Modal
driver.find_element_by_css_selector('button[data-cy=modal-delete-confirm-button').click()
time.sleep(5)
print('Hapus Item Activity')

#memastikan item terhapus
try:
    WebDriverWait(driver,10).until(EC.text_to_be_present_in_element((By.CSS_SELECTOR, 'div[data-cy=todo-item]'), 'Coba Item 1'))
    print('Item Belum Terhapus')

except TimeoutException:
    print('Item Dipastikan Terhapus')
    pass